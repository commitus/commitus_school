export const MSG = {
	invalid_credentials: "Invalid Credentials",
	invalid_token: "Invalid Token",
	err_global: "Something Went Wrong",
	token_expired: "Token Expired",
	no_data_found: "No Data Found.",

	//Student Validation error 
	firstName_cannot_empty : "First Name can't be empty",
	lastName_cannot_empty : "Last Name can't be empty",
	email_cannot_empty: "Email ID cannot be empty",
	email_invalid: "Email ID is invalid",
	mail_id_already_registered: "Email ID already registered in another account",
	password_cannot_empty: "Password cannot be empty",
	auth_reg_success: "Auth registration successful",
	stud_reg_success: "Registration successful",
	stud_profile_update_success: "Profile updated Successfully.",
	
}