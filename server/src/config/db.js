import mysql from 'mysql'

var connection = mysql.createConnection({
				  host: 'localhost',
				  user: 'root',
				  password: '',
				  database: 'school'
				});

connection.connect(function(err) {
    if (err) throw err;
});

module.exports = connection; 
