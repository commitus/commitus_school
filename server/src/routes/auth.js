import express from 'express';
import jwt from 'jsonwebtoken';
import { MSG } from '../shared/common/languages/english';
import { User } from '../models/user';
import conn from '../config/db';
import bcrypt from 'bcrypt';
import config from '../config';

const router = express.Router();
const USER = 'user';

router.post('/login', (req, res) => {
	const { email, password,userRole } = req.body.credentials;
     conn.query(" SELECT * FROM " + USER + " WHERE role = '" + userRole + "' AND email = '" + email + "'", function (err, result, fields) {
      if(result.length > 0){
	    if (err) {
	      	console.log(err)
	        res.status(500).json({ errors: { global: "Something went wrong" }});
	    } else {
			User.query({
			  where: {email: email, role:userRole} 
			}).fetch().then(User => { 
				if (User) { 
					console.log(bcrypt.hashSync(password, 10))
			    if (bcrypt.compareSync(password, User.get('password'))) {
			        const token = jwt.sign({
			          id 			: User.get('id'),
			          email_id 		: User.get('email'),
			          name 			: User.get('name'),
			          userType 		: userRole,
			        }, config.jwtSecret);
			        let userToken = {
			          token       : token,
			          userType 	  : userRole,
			          name 		  : User.get('name'),
			          id 		  : User.get('id'),
			          email_id	  : User.get('email'),
			        };
			        console.log(userToken)
			      res.json({ user: userToken});
			    } else { 
			      res.status(401).json({ errors: { global: MSG['invalid_credentials'] } });
			    }
			  } else { 
			    res.status(401).json({ errors: { global: MSG['invalid_credentials'] } });
			  }
		    });
		}
	  } else {
	  	res.status(401).json({ errors: { global: "Invalid credentials" } });
	  }
    })
});


export default router;