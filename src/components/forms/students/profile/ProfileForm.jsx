import React from 'react';
import propTypes from "prop-types";
import { MSG } from 'shared/common/languages/english';
import Validator from 'validator';
import NotificationSystem from 'react-notification-system';
import { config, notifyMsg } from 'config/constants';

class StudentProfileForm extends React.Component {
	state = {
		data: {
			firstName:"",
			lastName:"",
			password:"",
	      	address:"",
	      	city:"",
	      	state:"",
	      	uid: "",
	      	uidChildId: "",
		},
		errors: {},
		loading: false,
		success: false,
	}

	/*componentWillReceiveProps(nextProps) {
		let student = nextProps.student;
		this.setState({
			data: {
				firstName: (student.firstName) ? student.firstName : '',
				lastName: (student.lastName) ? student.lastName : '',
				address: (student.address) ? student.address : '',
				city: (student.city) ? student.city : '',
				state: (student.state) ? student.state : '',
				uid: (student.uid) ? student.uid : '',
				uidChildId: (student.uidChildId) ? student.uidChildId : ''
			}
		});
	} */
	
	handleChange = e => {
	    this.setState({
	      data: { ...this.state.data, [e.target.name]: e.target.value }
	    });
	}

	handleGenderChange = gender => {
        this.setState({
            data: { ...this.state.data, gender: JSON.stringify(gender)}
        });
    }
    
    validate = data => {
		const errors = {};
		if (Validator.isEmpty(data.firstName)) {
	  		errors.firstName = MSG['first_name_cannot_empty'];
		}
		if (Validator.isEmpty(data.lastName)) {
	  		errors.lastName = MSG['last_name_cannot_empty'];
		}
		if (Validator.isEmpty(data.address)) {
	  		errors.address = MSG['address_cannot_empty'];
		}
		if (Validator.isEmpty(data.city)) {
	  		errors.city = MSG['city_cannot_empty'];
		}
		if (Validator.isEmpty(data.state)) {
	  		errors.state = MSG['state_cannot_empty'];
		}
		
		return errors;
	};
/*
	onSubmit = (e) => {
		e.preventDefault();
		const errors = this.validate(this.state.data);
	    this.setState({ errors });
	    if (Object.keys(errors).length === 0) {
		    setTimeout(() => {
		        this.setState({ loading: true });
		    }, 5)
            this.props.updateStudentProfile(this.state.data).then((res) => {
            	// this.props.fetchStudent();
                this.setState({success: true, loading: false });
                this.refs.notificationSystem.addNotification(notifyMsg(MSG['profile_update_success'], "success")); 
              })
              .catch(err => {
               this.setState({ errors: err.response.data.errors, loading: false })
                var errMsg = err.response.data.errors.global;
                this.refs.notificationSystem.addNotification(notifyMsg(errMsg, "error")); 
            });
	    }
	}
	*/
/*	handleSubmit = (e) => {
  		e.preventDefault()
    	const errors = this.validate(this.state.data);
	    this.setState({ errors });
	    if (Object.keys(errors).length === 0) {
	       	this.props.updateStudentProfile(this.state.data)
	       	.then( res => {
       			 this.setState({success: true, loading: false });
       			// this.props.success()
       			this.refs.notificationSystem.addNotification(notifyMsg(MSG['profile_update_success'], "success")); 
	       	})
	        .catch(err => {
	          let globalErrors = {global: err.response.data.error.message, loading: false};
	          this.setState({ errors: globalErrors })
	        });
	    }
  	};*/

	render() {
		const { data, errors, success } = this.state;
		return(
		    <div>
		    <NotificationSystem ref="notificationSystem" style={config.notifyStyle}/>
				{errors.global && <div className="alert alert-danger text-center" style={{margin: "20px", borderRadius: "5px", padding: "15px"}}>{errors.global}</div> }
				<form onSubmit={this.handleSubmit}>
					<div className="container-fluid">
						<div className="form-group">
			            	<label htmlFor="">First Name<small className="str_clr">*</small></label>
						 	<input className="form-control" type="text" name="firstName" onChange={this.handleChange} placeholder="Enter First Name" value={(data.firstName) ? data.firstName : ''} maxLength="100"/>
		            		{errors.firstName && <span className="err-msg">{errors.firstName}</span>}
		            	</div>
		            	<div className="form-group">
			            	<label htmlFor="">Last Name<small className="str_clr">*</small></label>
			            	<input className="form-control" type="text"  name="lastName" onChange={this.handleChange} placeholder="Enter Last Name" value={(data.firstName) ? data.lastName : ''} maxLength="100"/>
	           				{errors.lastName && <span className="err-msg">{errors.lastName}</span>}
	           			</div>
	           			<div className="form-group">
			            	<label htmlFor="">Address<small className="str_clr">*</small></label>
						 	<input className="form-control" type="text" name="address" onChange={this.handleChange} placeholder="Enter Address" value={(data.address) ? data.address : ''} maxLength="200"/>
		            		{errors.address && <span className="err-msg">{errors.address}</span>}
		            	</div>
		            	<div className="form-group">
			            	<label htmlFor="">City<small className="str_clr">*</small></label>
			            	<input className="form-control" type="text" name="city" onChange={this.handleChange}  placeholder="Enter City" value={(data.city) ? data.city : ''} maxLength="50"/>
	           				{errors.city && <span className="err-msg">{errors.city}</span>}
	           			</div>
	           			<div className="form-group">
			            	<label htmlFor="">State<small className="str_clr">*</small></label>
			            	<input className="form-control" type="text" name="state" onChange={this.handleChange} placeholder="Enter state" value={(data.state) ? data.state : ''} maxLength="50"/>
	           				{errors.state && <span className="err-msg">{errors.state}</span>}
	           			</div>
	           		</div>
	           		<div className="footer text-center">
						<button type="submit" className="btn btn-primary mt-6">Update</button>
	            	</div>
	            </form>
        	</div>
		);
	}	
}

StudentProfileForm.propTypes = {
	student: propTypes.object.isRequired,
}

export default StudentProfileForm;
