import React from "react";
import PropTypes from "prop-types";
import Validator from "validator";
import { MSG } from 'shared/common/languages/english';
//import NotificationSystem from 'react-notification-system';
//import { config } from 'config/constants';

class SignUpForm extends React.Component {
  state = {
    data: {
      visitorName: '',
      email: '',
      mobile: '',
      city: '',
      company: '',
      designation: ''
    },
    loading: false,
    errors: {},
    invalid: false,
    success: false,
  };

  handleChange = (e) => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  }
    
    handleKeyPress = (event) =>{
       var regex = new RegExp("^[0-9+]*$"); 
       var key = String.fromCharCode(event.charCode ? event.which : event.charCode); 
       if (!regex.test(key)) { 
        event.preventDefault();
        this.setState({
          data: { ...this.state.data, [event.target.name]: event.target.value }
        });
         return false; 
       } 
    }
    


  handleSubmit = (e) => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      setTimeout(() => {
        this.setState({ loading: true });
      }, 5) 
        this.props.visitorSignupRequest(this.state.data)
        .then(res =>{
           this.props.signupSuccess(this.state.data);
        })
         .catch(err => {
           this.setState({ errors: err.response.data.errors, loading: false });
        })
       }
   };

  validate = data => {
    const errors = {};
    if (Validator.isEmpty(data.visitorName)) {
      errors.visitorName =  MSG['name_cannot_empty'];
    } else if (!Validator.isLength(data.visitorName, {min:3})) {
      errors.visitorName = MSG['name_length'];
    }
    if (Validator.isEmpty(data.email)) {
      errors.email = MSG['email_cannot_empty'];
    } else if (!Validator.isEmail(data.email)) {
      errors.email = MSG['email_invalid'];
    }
    if (Validator.isEmpty(data.mobile)) {
      errors.mobile = MSG['mobile_cannot_empty'];
    } else if(data.mobile.length < 10) {
      errors.mobile = MSG['mobile_length'];
    } else if (!Validator.matches(data.mobile, /^(\+[\d]{1,3}|0)?[6-9]\d{9}$/)) {
      errors.mobile = MSG['invalid_mobile'];
    }
    if (Validator.isEmpty(data.city)) {
      errors.city = MSG['city_cannot_empty'];
    } else if (!Validator.isLength(data.city, {min:2})) {
      errors.city = MSG['city_length'];
    }
    return errors;
  };

  render() {
    const { errors, loading } = this.state;
    const loadingDiv = ( <div className="loading"></div> )
    return (
     <div className="m-login__wrapper-2 m-portlet-full-height pt-50">
        { (!loading) ? loadingDiv : '' }
            <div className="m-login__contanier">
                <div className="m-login__signin">
                  <div className="m-login__head">
                    <div className="m-login__head">
                        <h3 className="m-login__title blk_clr">
                            <strong>Registration</strong>
                        </h3>
                    </div>
                    { errors.global && <div className="alert alert-danger text-center" style={{margin: "20px", borderRadius: "5px", padding: "15px"}}>{errors.global}</div> }
                    <form className="m-login__form m-form" onSubmit= {this.handleSubmit}>
                        <div className="form-group m-form__group">
                            <input className="form-control m-input" type="text" id="visitorName" onChange={this.handleChange} name="visitorName" placeholder="Enter your name *" maxLength="50"/>
                      
                    {errors.visitorName && <span className="messages">{errors.visitorName}</span>}
                        </div>
                        <div className="form-group m-form__group">
                            <input className="form-control m-input" type="email" id="email" onChange={this.handleChange}  name="email" placeholder="Enter Email ID*" maxLength="200"/>
                    {errors.email && <span className="messages">{errors.email}</span>}
                        </div>
                        <div className="form-group m-form__group">
                            <input className="form-control m-input" onKeyPress={this.handleKeyPress} type="text" id="mobile" onChange={this.handleChange} name="mobile" maxLength="13" placeholder="Enter mobile number *" />
                     {errors.mobile && <span className="messages">{errors.mobile}</span>}
                        </div>
                        <div className="form-group m-form__group">
                            <input className="form-control m-input" type="text" id="city" onChange={this.handleChange} name="city" placeholder="Enter city *"/>
                     {errors.city && <span className="messages">{errors.city}</span>}
                        </div>
                        <div className="form-group m-form__group">
                            <input className="form-control m-input" type="text" id="company" onChange={this.handleChange} name="company" placeholder="Enter company" />
                      {errors.company && <span className="messages">{errors.company}</span>}
                        </div>
                        <div className="form-group m-form__group">
                            <input className="form-control m-input" type="text" placeholder="Designation" id="designation" onChange={this.handleChange} name="Enter designation"/>
                      {errors.designation && <span className="messages">{errors.designation}</span>}
                        </div>
                        <div className="terms-conditions">By creating an account, you agree to the Terms and Conditions.</div>
                        <div className="m-login__form-action">
                            <button type="submit" id="m_login_signup_submit" className="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air btn-shd wht_clr">
                                Register
                            </button>
                            <a>
                             <button onClick={() => this.props.clickToLoginPage()} id="m_login_signup_cancel" className="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                                Cancel
                            </button>
                            </a>
                        </div>
                    </form>
                </div>
            
            </div>
        </div>
      </div>
    );
  }
}

SignUpForm.propTypes = {
  visitorSignupRequest: PropTypes.func.isRequired,
  isVisitorExists : PropTypes.func.isRequired,
};

export default SignUpForm;