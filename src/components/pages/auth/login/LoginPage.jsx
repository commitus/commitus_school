import React from 'react';
import { connect } from "react-redux";
import HeaderNavBar from 'components/pages/common/HeaderNavBar';
import LoginForm from 'components/forms/auth/login/LoginForm';
import { login } from 'actions/auth';

class LoginPage extends React.Component {
	 state = {
	 	roleMgt :''
	 }
	componentDidMount(){
		var role = this.props.match.params.type
		this.setState({
			roleMgt:role
		})
	}

	submit = (data) => {
		data['userRole'] = this.state.roleMgt
		return this.props.login(data)
    }
    
	render() {
		const {roleMgt} = this.state
		var userType = roleMgt.charAt(0).toUpperCase()+ roleMgt.slice(1)
		return (
            <div className="main-panel main-panel-1">
	            <HeaderNavBar />
	            <div className="content">
	                <div className="container-fluid">
	                    <div className="row">
	                    	<div className="col-md-4">
	                    	</div>
	                        <div className="col-md-4">
	                            <div className="card">
	                                <div className="card-content">
	                                    <h4 className="text-center">{userType} Login</h4>
	                                    <LoginForm submit={this.submit}/>
	                                </div>
	                            </div>
	                    	</div>
	                	</div>
	            	</div>
	        	</div>
	    	</div>
	    )
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        login: (data) => dispatch(login(data)),
    }
}

export default connect(null, mapDispatchToProps)(LoginPage)