import React from 'react';
import HeaderNavBar from 'components/pages/common/HeaderNavBar';
import { logout } from "actions/auth";
import { connect } from "react-redux";
import propTypes from "prop-types";
class LandingPage extends React.Component {
	componentDidMount() {
        this.props.logout();
    }
	render() {
		return (
            <div className="main-panel main-panel-1">
	            <HeaderNavBar />
	            <div className="content">
	                <div className="container-fluid">
	                    <div className="row">
	                        <div className="col-md-12 text-center">
                                <div class="card p-100" style={{width:'30rem'}} onClick ={()=>{
                                	this.props.history.push("/admin/login")
                                }}>
								    <div class="card-body">
									    <h5 class="card-title">ADMIN</h5>
								    </div>
								</div>
								<div class="card p-100" style={{width:'30rem'}} onClick ={()=>{
                                	this.props.history.push("/teachers/login")
                                }}>
								    <div class="card-body">
									    <h5 class="card-title">TEACHER</h5>
								    </div>
								</div>
								<div/>
								<div class="card p-100" style={{width:'30rem'}}onClick ={()=>{
                                	this.props.history.push("/students/login")
                                }}>
								    <div class="card-body">
									    <h5 class="card-title">STUDENTS</h5>
								    </div>
								</div>
								<div class="card p-100" style={{width:'30rem'}} onClick ={()=>{
                                	this.props.history.push("/parents/login")
                                }}>
								    <div class="card-body">
									    <h5 class="card-title">PARENTS</h5>
								    </div>
								</div>
	                    	</div>
	                	</div>
	            	</div>
	        	</div>
	    	</div>
	    )
	}
}


LandingPage.propTypes = {
    logout: propTypes.func.isRequired
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logout())
  }
}

export default connect(null, mapDispatchToProps)(LandingPage);