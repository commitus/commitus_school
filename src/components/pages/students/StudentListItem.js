import React from 'react';
import propTypes from 'prop-types';

class StudentListItem extends React.Component {
	render () {
		const { student } = this.props;
		return (
			<tr >
				<td className="td-actions">{student.id}</td>
				<td className="td-actions">{student.first_name}</td>
				<td className="td-actions">{student.last_name}</td>
				<td className="td-actions"><img src={student.avatar} height="100" width="100"/></td>
			</tr>
		);
	}
}

StudentListItem.propTypes = {
	student: propTypes.object.isRequired
}

export default StudentListItem;