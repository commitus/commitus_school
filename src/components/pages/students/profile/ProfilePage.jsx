import React from 'react';
import ProfileForm from 'components/forms/students/profile/ProfileForm';
import { connect } from "react-redux";
import { updateStudentProfile, fetchStudent } from "actions/students";
import decode from 'jwt-decode';
import propTypes from "prop-types";
import HeaderNavBar from "components/pages/common/HeaderNavBar";

class ProfilePage extends React.Component {
	state = {
		data : {},
		userId: ''
	}

	/*componentDidMount() {
		window.scrollTo(0,0);
		if (localStorage.zwaarJWT) {
            const userData = decode(localStorage.zwaarJWT);
            if(userData.id) {
            	this.setState({ userId: userData.id })
            	setTimeout(function () {
            		this.props.fetchStudent(this.state.userId)
            	}.bind(this), 5)
            }
        }
	}*/

	/*updateStudentProfile = data => { 
		return this.props.updateStudentProfile(data);
	}*/
	
	/*fetchStudent = () => {
         this.props.fetchStudent(this.state.userId)
	}
	*/
	render()
	{ 
   		return(
		    	<div className="main-panel main-panel-1">
	            <HeaderNavBar />
	            <div className="content">
	                <div className="container-fluid">
	                    <div className="row">
	                    	<div className="col-md-4">
	                    	</div>
	                        <div className="col-md-4">
	                            <div className="card">
	                                <div className="card-content">
	                                    <h4 className="text-center">Profile</h4>
	                                    <ProfileForm />
	                                </div>
	                            </div>
	                    	</div>
	                	</div>
	            	</div>
	        	</div>
	    	</div>
		);
	}
}

ProfilePage.propTypes = {
	updateStudentProfile: propTypes.func.isRequired,
	fetchStudent: propTypes.func.isRequired,
	student: propTypes.object.isRequired,
}

const mapDispatchToProps = (dispatch) => {
	return {
		fetchStudent: (id) => dispatch(fetchStudent(id)),
		updateStudentProfile: (data) => dispatch(updateStudentProfile(data)),
	}
}

const mapStateToProps = (state) => {	
	let student = (state.students.student) ? (state.students.student) : {};
	console.log(student)
	return {
		student
	}
}

export default connect(null, null)(ProfilePage);