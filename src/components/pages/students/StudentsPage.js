import React from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";
import StudentsList from "components/pages/students/StudentsList";
//import { fetchStudents } from "actions/students";
import HeaderNavBar from "components/pages/common/HeaderNavBar";

class StudentsPage extends React.Component {

	/*componentDidMount() {
	   this.props.fetchStudents();
    }*/

	render() {
		let { students } = this.props;
		return(
            <div className="main-panel main-panel-1">
            <HeaderNavBar />
                <div className="content content-left">
                    <div className="container-fluid">
                        <div className="row">
                        	<div className="col-md-12">
                            	<StudentsList students={students} />
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
		);
	}
}

StudentsPage.propTypes = {
	fetchStudents: propTypes.func.isRequired,
}

/*const mapStateToProps = (state) => {
    let students = (state.students.studentsList) ? state.students.studentsList : {}
    console.log(students);
    return {
        students
    }
}*/

/*const mapDispatchToProps = (dispatch) => {
	return {
		fetchStudents: (data) => dispatch(fetchStudents(data))
	}
}*/

export default connect(null, null)(StudentsPage);