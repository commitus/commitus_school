import React from 'react';
import propTypes from 'prop-types';
import StudentListItem from 'components/pages/students/StudentListItem';

class StudentsList extends React.Component {
	render() {
		const { students } = this.props;
		return(
            <div className="card">
              <div className="card-content">
                  <div className="table-responsive">
                      <table className="table">
                          <thead>
                              <tr>
                                  <th width="30%">#</th>
                                  <th width="30%">First Name</th>
                                  <th width="30%">Last City</th>
                                  <th width="30%">Profile Photo</th>
                              </tr>
                          </thead>
                          	<tbody>
                            {/*(students.length) 
                            ? students.map((student, index) => <StudentListItem key={index} student={student} />) 
	                        	: <tr><td colSpan="4">No Data Found.</td></tr>*/
                        	}
                        	</tbody>
                      </table>
                    </div>
              </div>
          	</div>
		);
	}
}

StudentsList.propTypes = {
	students: propTypes.array.isRequired
}

export default StudentsList;