import { combineReducers } from 'redux';
import auth from 'reducers/auth';
import students from 'reducers/students';

export default combineReducers({
	auth,
	students
});