import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

const GuestRoute = ({ isAuthenticated, userType, component: Component, ...rest }) => (
 <Route
   {...rest}
   render={props =>
     !isAuthenticated ? (
       <Component {...props} />
     ) : (
       <Redirect to="/dashBoard" />
     )}
 />
);

GuestRoute.propTypes = {
 component: PropTypes.func.isRequired,
 isAuthenticated: PropTypes.bool.isRequired,
 userType:PropTypes.string.isRequired
};

function mapStateToProps(state) {
	console.log(state.auth.loggedUser)
 return {
 	userType: (state.auth.loggedUser.userType) ? state.auth.loggedUser.userType : '',
   isAuthenticated: (Object.keys(state.auth.loggedUser).length > 0 || localStorage.schoolJWT) ? true : false
 };
}

export default connect(mapStateToProps)(GuestRoute);
