import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router-dom";
import UserRoute from "routes/UserRoute";
import GuestRoute from "routes/GuestRoute";

import 'assets/css/font-awesome.min.css';
import 'assets/css/style.css';
import 'assets/css/font.css';
import 'assets/css/common.css';

import LoginPage from "components/pages/auth/login/LoginPage";
import LandingPage from "components/pages/LandingPage";
import DashBoardPage from "components/pages/DashBoardPage";
import NoMatchPage from "components/pages/NoMatchPage";

const App = ({ location }) => (
    <div>
      <Switch>
        <Route location={location} path="/" exact component={LandingPage} />
        <Route location={location} path="/:type/login" exact component={LoginPage} />
        <Route location={location} path="/dashBoard" exact component={DashBoardPage} />
        <Route location={location} component={NoMatchPage} />
      </Switch>
    </div>
);

App.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired
};

export default App;
