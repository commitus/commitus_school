export const MSG = {
	email_cannot_empty: "Email ID cannot be empty",
	email_invalid: "Email ID is Invalid",
	password_cannot_empty: "Password cannot be empty",
	email_already_exist: "Email ID already exists",
	first_name_cannot_empty: "First name cannot be empty",
	last_name_cannot_empty: "Last name cannot be empty",
	address_cannot_empty: "Address cannot be empty",
	city_cannot_empty: "City cannot be empty",
	state_cannot_empty: "State cannot be empty",
	gender_cannot_empty: "Gender cannot be empty",
	pincode_cannot_empty: "Pincode cannot be empty",
	profile_update_success: "Profile updated successfully.",
}