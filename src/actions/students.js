import axios from 'axios';

export const STUDENTS_FETCHED = 'STUDENTS_FETCHED';
export const STUDENT_FETCHED = 'STUDENT_FETCHED';

/*=========== Reducer Functions begin ===========*/

export const studentsFetched = students => ({
    type: STUDENTS_FETCHED,
    students
});

export const studentFetched = student => ({
    type: STUDENT_FETCHED,
    student
});

/*=========== Reducer Functions end ===========*/

/*=========== Dispatch Functions begin ===========*/

export const fetchStudents = (data) => dispatch => {
    axios.get('/api/students/fetchStudents', {data})
    .then(res => {
        let objResult = JSON.parse(res.data);
        dispatch(studentsFetched(objResult.data));
    });
}

export const fetchStudent = (id)  => dispatch => {
    axios.get(`/api/students/fetchStudent/${id}`)
    .then(res => {
    dispatch(studentFetched(res.data.result));
        return res.data.result;
    });
}

export function updateStudentProfile(data) {
    return dispatch => {
        return axios.post('/api/students/updateStudentProfile', {data});
    }
}

/*=========== Dispatch Functions end ===========*/