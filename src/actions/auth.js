import axios from "axios";

export const STUDENT_LOGGED_IN = "STUDENT_LOGGED_IN";
export const STUDENT_LOGGED_OUT = "STUDENT_LOGGED_OUT";

//=========== Reducer call functions begin ===========//
export const studentLoggedIn = student => ({
  type: STUDENT_LOGGED_IN,
  student
});

export const studentLoggedOut = () => ({
  type: STUDENT_LOGGED_OUT
});

//=========== Reducer call functions begin ===========//

//=========== student routing begin ===========//
/*export const validateStudentLoggedIn = student => dispatch => {
  dispatch(studentLoggedIn(student))
  axios.post("/api/auth/validateStudentLoggedIn", { student })
  .catch(err => {
    localStorage.removeItem("zwaarJWT");
    dispatch(studentLoggedOut());
  })
};*/

export const login = credentials => dispatch => {
  return axios.post("/api/auth/login", {credentials} )
  .then(res => {
    console.log(res.data.user)
    localStorage.schoolJWT = res.data.token;
    dispatch(studentLoggedIn(res.data.user));
    return res.data.user;
  });
};

export const logout = () => dispatch => {
  localStorage.removeItem("zwaarJWT");
  dispatch(studentLoggedOut());
};

/*export const register = (data) => dispatch => {
  return axios.post("/api/auth/studentsRegisterationAuth", { data })
  .then( res => {
    data.uid = res.data.uid;
    return axios.post("/api/auth/studentsRegisteration", { data });
  })
};*/
//=========== student routing end ===========//