import { STUDENT_LOGGED_IN, STUDENT_LOGGED_OUT } from 'actions/auth';

const initialState = {
  isAuthenticated: false,
  loggedUser: {}
}

export default function auth(state = initialState, action = {}) {
  switch (action.type) {
      case STUDENT_LOGGED_IN:
        return Object.assign({}, state, {
            loggedUser: action.student
        })

      case STUDENT_LOGGED_OUT:
          return {}

    default:
      return state;
    }
}