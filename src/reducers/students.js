import { STUDENT_FETCHED, STUDENTS_FETCHED } from 'actions/students';

const initialState = {
	studentsList: [],
	student : {}
}

export default function students(state = initialState, action = {}) {
	switch(action.type) {
		case STUDENTS_FETCHED: {
    		return { ...state, studentsList: action.students }
    	}
	   	case STUDENT_FETCHED :{
			return { ...state, student:  action.student }
		}
	
		default: return state
	}
}